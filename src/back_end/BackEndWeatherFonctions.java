package back_end;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import dao.weather.WeatherDao;
import entities.Weather;


public class BackEndWeatherFonctions 
{
	
	public static void UpdateWeatherInformation()
	{
		WeatherDao weatherDAO = new WeatherDao();
		weatherDAO.ClearWeatherInformations();
		Object informations = "";
		try {
			informations = getWeatherFromAPI().get("query");
			informations = ((JSONObject)informations).get("results");
			informations = ((JSONObject)informations).get("channel");
			informations = ((JSONObject)informations).get("item");
			informations = ((JSONObject)informations).get("forecast");
			informations =  informations.toString().substring(2,informations.toString().length()-2);
			String[] forecasts = ((String) informations).split("\\}\\,\\{");
			 for (int i = 0; i < forecasts.length; i++) 
			 {
				System.out.println(forecasts[i]);
				
				String[] forcastTemp = forecasts[i].split("\",\"");
				String sDate = forcastTemp[0].substring(forcastTemp[0].indexOf(":")+2, forcastTemp[0].length());
				String sTempMax = forcastTemp[1].substring(forcastTemp[1].indexOf(":")+2, forcastTemp[1].length());
				String sTempMin = forcastTemp[3].substring(forcastTemp[3].indexOf(":")+2, forcastTemp[3].length());
				String weatherType = forcastTemp[4].substring(forcastTemp[4].indexOf(":")+2, forcastTemp[4].length());
				String[] dateInformations = sDate.split(" ");			
				Date temp = new SimpleDateFormat("MMM", Locale.ENGLISH).parse(dateInformations[1]);
				Calendar cal = Calendar.getInstance();
			    cal.setTime(temp);
			    int month = cal.get(Calendar.MONTH)+1;
			    DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
			    Date date =dateFormat1.parse(dateInformations[2] + "-" + month + "-" + dateInformations[0]);
			    Weather w = new Weather(date,Integer.valueOf(sTempMax).intValue(),Integer.valueOf(sTempMin).intValue(),weatherType);			    
			    weatherDAO.addWeatherInformation(w);
		     }
		} catch (JSONException e) 
		{
			e.printStackTrace();
		} catch (Exception e) 
		{
			e.printStackTrace();
		}
		
	}
	
	public static JSONObject getWeatherFromAPI() throws Exception 
	{
		String api = "http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid=12727257%20and%20u%20=%20'c'";
		System.out.println("api = "+api);
		JSONObject json = null;
			
		try {

			URL url = new URL(api);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));

			StringBuilder sb = new StringBuilder();
			br.lines().forEach(sb::append);
			json = new JSONObject(sb.toString());
			System.out.println("Output from Server .... \n");
				
				
			conn.disconnect();
			} catch (MalformedURLException e)
		{
				e.printStackTrace();
				}
		return json;
	}}