package back_end;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class BackEndTask extends TimerTask implements ServletContextListener
{

	@Override
	public void run() 
	{
		BackEndWeatherFonctions.UpdateWeatherInformation();
	}

	@Override
	public void contextDestroyed(ServletContextEvent arg0) 
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) 
	{
		TimerTask task = new BackEndTask();
        Calendar calendar= Calendar.getInstance();    
        Date firstTime = calendar.getTime();        
        long period = 1000 * 60 * 60 * 3;           	        
        Timer timer = new Timer();        
        timer.schedule(task,firstTime,period);
		
	}
	
}
