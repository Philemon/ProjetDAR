package controllers.user;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import controllers.util.ServletsConfig;
import services.UserServices;

@WebServlet(name = "SaveUserFingerprinting", urlPatterns = { "/saveUserFingerprinting" })
public class SaveUserFingerprinting extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) {
		// Récupération des paramètres de la requête
		String platform = request.getParameter(ServletsConfig.PLATFORM);
		String resolution = request.getParameter(ServletsConfig.RESOLUTION);
		String userAgent = request.getParameter(ServletsConfig.USER_AGENT);
		String canvasFingerprinting = request.getParameter(ServletsConfig.CANVAS_FINGERPRINTING);
		
       System.out.println(platform+" "+ resolution+" "+userAgent+" "+canvasFingerprinting);
		try {
			// Traitement
			JSONObject json = UserServices.saveUserFingerprinting(platform, resolution, userAgent, canvasFingerprinting);

			// Envoi de la réponse
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().print(json.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
