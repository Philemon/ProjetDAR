package controllers.user;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import controllers.util.ServletsConfig;
import services.UserServices;

@WebServlet(name = "GetUser", urlPatterns = { "/user" })
public class GetUser extends HttpServlet {

	private static final long serialVersionUID = 2199789224984531035L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) {
		// Récupération des paramètres de la requête
		String sessionKey = request.getParameter(ServletsConfig.SESSION_KEY);

		try {
			// Traitement
			JSONObject json = UserServices.getUserInfo(sessionKey);

			// Envoi de la réponse
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().print(json.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
