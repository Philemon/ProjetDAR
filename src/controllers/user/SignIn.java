package controllers.user;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import controllers.util.ServletsConfig;
import services.UserServices;

@WebServlet(name = "SignIn", urlPatterns = { "/signin" })
public class SignIn extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) {
		// Récupération des paramètres de la requête
		String login = request.getParameter(ServletsConfig.LOGIN);
		String password = request.getParameter(ServletsConfig.PASSWORD);

		try {
			// Traitement
			JSONObject json = UserServices.signIn(login, password);
			
			// création des cookies et stockage de la clée de session
			
			Cookie cookie = new Cookie("sessionKey", (String)json.get("sessionKey"));
			cookie.setHttpOnly(true);
			cookie.setSecure(true);
			response.addCookie(cookie);
						
			//System.out.println("cookies..."+cookie.getName());
						
			// Envoi de la réponse
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().print(json.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
