package controllers.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import controllers.util.ServletsConfig;
import services.VisitorServices;

@WebServlet(name = "ParticipateInAVisit",
		urlPatterns = { "/participateInAVisit" })
public class ParticipateInAVisit extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// Récupération des paramètres de la requête
		String visitId = request.getParameter(ServletsConfig.ID);
		String sessionKey = request.getParameter(ServletsConfig.SESSION_KEY);

		try {
			// Traitement
			JSONObject json =
					VisitorServices.participateInAVisit(sessionKey, visitId);
			
			// Envoi de la réponse
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().print(json.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
