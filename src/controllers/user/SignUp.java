package controllers.user;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import controllers.util.SecurityUtils;
import controllers.util.ServletsConfig;
import services.UserServices;

@WebServlet(name = "SignUp", urlPatterns = { "/signup" })
public class SignUp extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) {
		// Récupération des paramètres de la requête
		String firstName = request.getParameter(ServletsConfig.FIRST_NAME);
		String lastName = request.getParameter(ServletsConfig.LAST_NAME);
		String emailAddress =
				request.getParameter(ServletsConfig.EMAIL_ADDRESS);
		String phoneNumber = request.getParameter(ServletsConfig.PHONE_NUMBER);
		String password = request.getParameter(ServletsConfig.PASSWORD);
		String isGuide = request.getParameter(ServletsConfig.IS_GUIDE);
		
		firstName = SecurityUtils.secure(firstName);
		lastName = SecurityUtils.secure(lastName);
		emailAddress = SecurityUtils.secure(emailAddress);
		phoneNumber = SecurityUtils.secure(phoneNumber);
		password = SecurityUtils.secure(password);
		isGuide = SecurityUtils.secure(isGuide);

		try {
			// Traitement
			JSONObject json = UserServices.signUp(emailAddress, password,
					firstName, lastName, phoneNumber, isGuide);

			// Envoi de la réponse
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().print(json.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
