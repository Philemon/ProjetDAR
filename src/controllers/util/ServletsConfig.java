package controllers.util;

public class ServletsConfig {

	public static final String ID = "id";

	public static final String PICTURE_ID = "id";

	public static final String LIST_OF_PICTURE = "listOfPicture";

	public static final String PASSWORD = "password";

	public static final String EMAIL_ADDRESS = "emailAddress";

	public static final String PHONE_NUMBER = "phoneNumber";

	public static final String FIRST_NAME = "firstName";

	public static final String LAST_NAME = "lastName";

	public static final String BIRTHDATE = "birthdate";

	public static final String IS_GUIDE = "isGuide";

	public static final String VISIT_NAME = "name";

	public static final String VISIT_LOCATION = "location";

	public static final String VISIT_DATE = "date";

	public static final String VISIT_DESCRIPTION = "description";

	public static final String VISIT_WEATHER = "weather";

	public static final String VISIT_NUMBER = "visitNumber";

	public static final String VISIT_PICTURE = "file";

	public static final String LOGIN = "login";

	public static final String MARK = "mark";

	public static final String CONTENT = "content";

	public static final String PROFILE_PICTURE = "profilePicture";

	public static final String BIRTH_DATE = "birthDate";

	public static final String PRESENTATION = "presentation";

	public static final String PAYPAL_ACCOUNT = "paypalAccount";

	public static final String IS_APPROUVED = "isApprouved";

	public static final String USER_TYPE = "type";

	public static final String SUCCESS = "success";

	public static final String ERROR = "error";
	
	public static final String SESSION_KEY = "sessionKey";

	public static final String LOCATION_NAME = "locName";

	public static final String LOCATION_LAT = "lat";
	
	public static final String LOCATION_LNG = "lng";

	public static final String VISIT_LIST = "visits";

	public static final String TYPE_TEMP = "typeTemp";
	
	public static final String MIN_TEMP = "minTemp";
	
	public static final String MAX_TEMP = "maxTemp";
	
	public static final String PLATFORM = "platform";

	public static final String RESOLUTION = "resolution";
	
	public static final String USER_AGENT = "userAgent";

	public static final String CANVAS_FINGERPRINTING = "canvasFingerprinting";

	public static final String API_KEY = "apiKey";

	public static final String HOST = "host";

	public static final String CSRF = "csrf";
	
	private ServletsConfig() {}

}
