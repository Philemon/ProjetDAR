package controllers.util;

import org.apache.commons.lang3.StringEscapeUtils;

public class SecurityUtils {

	/**
	 * Returns a secure version of the given string.
	 * 
	 * @param string
	 *            the string
	 * @return a secure version of the string
	 */
	public static String secure(String string) {
		String parameter = string;
		parameter = StringEscapeUtils.escapeHtml4(string);
		parameter = StringEscapeUtils.escapeJson(parameter);

		return parameter;
	}

}
