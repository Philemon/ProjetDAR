package controllers.host;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.tuple.Pair;

import controllers.util.ServletsConfig;
import services.HostServices;
import services.util.Javascript;

@WebServlet(name = "GetCsrfToken", urlPatterns = { "/get-csrf-token.js" })
public class GetCsrfToken extends HttpServlet {

	private static final long serialVersionUID = -4743104229772310613L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) {
		String apiKey = request.getParameter(ServletsConfig.API_KEY);
		Cookie csrfCookie = getCookieByName(request, ServletsConfig.CSRF);
		String secureRandom = csrfCookie != null ? csrfCookie.getValue() : null;

		try {
			Pair<Javascript, String> jsAndRandom =
					HostServices.getCsrfToken(apiKey, secureRandom);

			Javascript js = jsAndRandom.getLeft();
			secureRandom = jsAndRandom.getRight();

			Cookie cookie = new Cookie(ServletsConfig.CSRF, secureRandom);
			cookie.setHttpOnly(true);

			response.setContentType("application/javascript");
			response.setCharacterEncoding("UTF-8");
			response.setHeader("Cache-Control",
					"no-cache, no-store, must-revalidate");
			response.setHeader("Pragma", "no-cache");
			response.setDateHeader("Expires", 0);
			response.addCookie(cookie);

			response.getWriter().print(js.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static Cookie getCookieByName(HttpServletRequest request,
			String cookieName) {
		Cookie[] cookies = request.getCookies();

		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals(cookieName)) {
					return cookie;
				}
			}
		}

		return null;
	}

}
