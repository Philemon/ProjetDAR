package controllers.host;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import controllers.util.ServletsConfig;
import services.HostServices;

@WebServlet(name = "Register", urlPatterns = { "/register" }) // register?host=localhost
public class Register extends HttpServlet {

	private static final long serialVersionUID = -6887079976036756340L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) {
		// Récupération des paramètres de la requête
		String host = request.getParameter(ServletsConfig.HOST);

		try {
			// Traitement
			JSONObject json = HostServices.register(host);

			// Envoi de la réponse
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().print(json.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
