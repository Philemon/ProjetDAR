package controllers.visit;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import controllers.util.ServletsConfig;
import services.VisitorServices;

@WebServlet(name = "VisitPicture", urlPatterns = { "/visit-picture" })
public class VisitPicture extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// Récupération des paramètres
		String id = request.getParameter(ServletsConfig.ID);

		try {
			// Appel du service
			JSONObject json = VisitorServices.getVisitPicture(id);
			
			// Envoi de la réponse
			response.setContentType("application/json");
			response.getWriter().print(json.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
