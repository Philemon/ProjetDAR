package controllers.visit;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import controllers.util.ServletsConfig;
import services.UserServices;
import services.VisitorServices;

@WebServlet(name = "SignOut", urlPatterns = { "/signout" })
public class SignOut extends HttpServlet {

	private static final long serialVersionUID = 4044781484662967381L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) {
		// Récupération des paramètres
		String sessionKey = request.getParameter(ServletsConfig.SESSION_KEY);
		
		try {
			// Traitement
			JSONObject json = UserServices.signOut(sessionKey);

			// Envoi de la réponse
			response.setContentType("application/json");
			response.getWriter().print(json.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
