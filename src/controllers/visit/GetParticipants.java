package controllers.visit;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import controllers.util.ServletsConfig;
import services.VisitorServices;

@WebServlet(name = "GetParticipants", urlPatterns = { "/getParticipants" })
public class GetParticipants extends HttpServlet {

	private static final long serialVersionUID = 4044781484662967381L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) {
		// Récupération des paramètres
		String sessionKey = request.getParameter(ServletsConfig.SESSION_KEY);

		try {
			// appeler le service
			JSONObject json =
					VisitorServices.findAllParticipantsForVisit(sessionKey);

			// Envoi de la réponse
			response.setContentType("application/json");
			response.getWriter().print(json.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
