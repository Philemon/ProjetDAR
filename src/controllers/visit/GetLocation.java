package controllers.visit;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import services.VisitorServices;

@WebServlet(name = "GetLocation", urlPatterns = { "/getLocation" })
public class GetLocation extends HttpServlet {

	// créer un objet de VisitorServices

	private static final long serialVersionUID = 4044781484662967381L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) {
		// Récupération des paramètres
		String location = request.getParameter("location");

		try {
			// Traitement
			JSONObject json = VisitorServices.getLocation(location);
			
			// Envoi de la réponse
			response.setContentType("application/json");
			response.getWriter().print(json.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
