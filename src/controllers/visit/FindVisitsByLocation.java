package controllers.visit;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import services.VisitorServices;

@WebServlet(name = "FindVisitsByLocation",
		urlPatterns = { "/findVisitsByLocation" })
public class FindVisitsByLocation extends HttpServlet {

	private static final long serialVersionUID = 4044781484662967381L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) {
		// Récupération des paramètres
		String location = request.getParameter("location");

		try {
			// Traitement
			JSONObject json = VisitorServices.findVisitByLocation(location);
			
			// Envoi de la réponse
			response.setContentType("application/json");
			response.getWriter().print(json.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
