package controllers.visit;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import controllers.util.ServletsConfig;
import services.VisitorServices;

@WebServlet(name = "DeleteVisit", urlPatterns = { "/deleteVisit" })
public class DeleteVisit extends HttpServlet {

	private static final long serialVersionUID = 4044781484662967381L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) {
		// Récupération des paramètres
		String id = request.getParameter(ServletsConfig.ID);
		
		try {
			// Traitement
			JSONObject json = VisitorServices.deleteVisit(id);

			// Envoi de la réponse
			response.setContentType("application/json");
			response.getWriter().print(json.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
