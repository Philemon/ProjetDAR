package controllers.visit;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import controllers.util.ServletsConfig;
import services.VisitServices;

@WebServlet(name = "Visit", urlPatterns = { "/visit" })
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2,
		maxFileSize = 1024 * 1024 * 10, maxRequestSize = 1024 * 1024 * 50)
public class Visit extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// Récupération des paramètres
		String sessionKey = request.getParameter(ServletsConfig.SESSION_KEY);
		String name = request.getParameter(ServletsConfig.VISIT_NAME);
		String location = request.getParameter(ServletsConfig.VISIT_LOCATION);
		String lng = request.getParameter("lng");
		String lat = request.getParameter("lat");
		String date = request.getParameter(ServletsConfig.VISIT_DATE);
		String description =
				request.getParameter(ServletsConfig.VISIT_DESCRIPTION);

		String appPath = request.getServletContext().getRealPath("");
		try {
			// Traitement
			JSONObject json = VisitServices.addVisit(name, location, lng, lat,
					date, description, sessionKey, request.getParts(), appPath);

			// Envoi de la réponse
			response.setContentType("application/json");
			response.getWriter().print(json.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
