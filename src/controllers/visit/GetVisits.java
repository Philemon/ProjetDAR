package controllers.visit;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import controllers.util.ServletsConfig;
import services.VisitorServices;

@WebServlet(name = "GetVisits", urlPatterns = { "/getVisits" })
public class GetVisits extends HttpServlet {

	private static final long serialVersionUID = 4044781484662967381L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) {
		String sessionKey = request.getParameter(ServletsConfig.SESSION_KEY);

		try {
			JSONObject json = VisitorServices.findAllVisitsForGuide(sessionKey);

			// Envoi de la réponse
			response.setContentType("application/json");
			response.getWriter().print(json.toString());

		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("erreur : " + e.getMessage());
		}
	}
}
