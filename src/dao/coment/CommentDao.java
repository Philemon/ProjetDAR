package dao.coment;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import entities.Comment;
import entities.Guide;
import entities.User;
import entities.Visit;

public class CommentDao {

	private static EntityManagerFactory emf =
			Persistence.createEntityManagerFactory("ProjetDAR");
	EntityManager em = emf.createEntityManager();
	EntityTransaction tx = em.getTransaction();
	
	
	
	public Comment addComment(Comment c, Long visitId, Long userId) {
		User u = null;
		Visit v = null;
		tx.begin();
		if (visitId!=null) {
			 v = em.find(Visit.class, visitId);
			if(v==null) throw new RuntimeException("Visit not found");
		}
		if (userId!=null) {
			 u = em.find(User.class, userId);
			if(u==null) throw new RuntimeException("user not found");
		}
		c.setVisit(v);
		c.setUser(u);
		// create new entity manager 
		EntityManager em1 = emf.createEntityManager();
		// create new transaction
		EntityTransaction tx1 = em1.getTransaction();
		// begin new transaction
		tx1.begin();
		em1.persist(c);
		tx1.commit();
		return c;
	}
				
	
	
	

	public void updateComment(Comment c) {
		tx.begin();
		em.merge(c);
		tx.commit();
		
	}

	public void deleteComment(Long commentId) {
		tx.begin();
		 Query query = em.createQuery("delete Comment where id = :ID");
		 query.setParameter("ID", commentId);
		 int result = query.executeUpdate();
		 tx.commit();
		
	}
	

	public Comment getCommentById(Long commentId) {
		tx.begin();
		Comment c = em.find(Comment.class, commentId);
		if(c==null) throw new RuntimeException("Comment not found");
		// create new entity manager 
		EntityManager em1 = emf.createEntityManager();
		// create new transaction
		EntityTransaction tx1 = em1.getTransaction();
		// begin new transaction
		tx1.begin();
		//persist and commit
		tx1.commit();
		return c;
	}

	
}
