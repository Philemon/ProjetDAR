package dao.weather;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import entities.Weather;




public class WeatherDao implements IWeatherDao
{

	private static EntityManagerFactory emf =
			Persistence.createEntityManagerFactory("ProjetDAR");
	EntityManager em = emf.createEntityManager();
	EntityTransaction tx = em.getTransaction();


	public Weather getWeatherByDate(Date date) 
	{
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		Weather w = em.find(Weather.class, date);
		tx.commit();
		return w;	
	}
	

	public void addWeatherInformation(Weather w) 
	{
		tx.begin();
		em.persist(w);
		tx.commit();
	}
	
	public void ClearWeatherInformations()
	{
		tx.begin();
		Query q1 = em.createQuery("DELETE FROM Weather");
		q1.executeUpdate();
		tx.commit();
	}

	
	
}