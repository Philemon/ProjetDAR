package dao.weather;

import java.util.Date;

import entities.Weather;

public interface IWeatherDao 
{
	public Weather getWeatherByDate(Date date);
	
	public void addWeatherInformation(Weather w);
	
	public void ClearWeatherInformations();
	
}
