package dao.visit_picture;


import entities.*;

public interface IVisitPictureDao {

	public VisitPicture addVisitPicture(VisitPicture vp, Long visitId) throws IllegalArgumentException;
	public void changeVisitPicture(VisitPicture vp);
	public void deleteVisitPicture(Long visitPictureId);
	public VisitPicture getVisitPictureById(Long id) throws IllegalArgumentException;
}
