package dao.visit_picture;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import entities.Guide;
import entities.Visit;
import entities.VisitPicture;

public class VisitPictureDao implements IVisitPictureDao {

	private static EntityManagerFactory emf =
			Persistence.createEntityManagerFactory("ProjetDAR");

	@Override
	public VisitPicture addVisitPicture(VisitPicture vp, Long visitId)
			throws IllegalArgumentException {
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();

		Visit v = null;
		tx.begin();
		if (visitId != null) {
			v = em.find(Visit.class, visitId);
			if (v == null)
				throw new IllegalArgumentException();
		}

		vp.setVisit(v);
		EntityManager em1 = emf.createEntityManager();
		EntityTransaction tx1 = em1.getTransaction();

		tx1.begin();
		vp = em1.merge(vp);
		em1.persist(vp);
		tx1.commit();

		return vp;
	}

	@Override
	public void changeVisitPicture(VisitPicture vp) {
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		em.merge(vp);
		tx.commit();

	}

	@Override
	public void deleteVisitPicture(Long visitPictureId) {
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		Query query = em.createQuery("delete VisitPicture where id = :ID");
		query.setParameter("ID", visitPictureId);
		int result = query.executeUpdate();
		tx.commit();
	}

	@Override
	public VisitPicture getVisitPictureById(Long visitPictureId) throws IllegalArgumentException {
		EntityManager em = emf.createEntityManager();

		VisitPicture vp = em.find(VisitPicture.class, visitPictureId);
		if (vp == null)
			throw new IllegalArgumentException();

		return vp;
	}

}
