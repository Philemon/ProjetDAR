package dao.user;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import entities.Session;

public class SessionDao {

	private static EntityManagerFactory emf =
			Persistence.createEntityManagerFactory("ProjetDAR");
	private EntityManager em = emf.createEntityManager();
	private EntityTransaction tx = em.getTransaction();

	public Session getSessionBySessionKey(String sessionKey) {
		
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		Session session = null;
		
		tx.begin();
		Query query =
				em.createQuery("select s from Session s where s.sessionKey=:x");
		query.setParameter("x", sessionKey);
		tx.commit();
		
		if (query != null)
			session = (Session) query.getResultList().get(0);

		return session;
	}
	
	public Session addSession(Session s) {
		EntityManager em1 = emf.createEntityManager();
		EntityTransaction tx1 = em1.getTransaction();
		tx1.begin();
		s = em1.merge(s);
		em1.persist(s);
		tx1.commit();
		return s;
	}
	
	public void deleteSession(Session s) {
		tx.begin();
		em.remove(s);
		tx.commit();
	}

}
