package dao.user;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import entities.Fingerprinting;
import entities.Guide;
import entities.Session;
import entities.User;

public class UserDao {

	private static EntityManagerFactory emf =
			Persistence.createEntityManagerFactory("ProjetDAR");

	public User signIn(String emailAddress, String password) {
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		User u = null;
		tx.begin();
		Query req = em.createQuery(
				"select u from User u where u.emailAddress=:x and u.password=:y");
		req.setParameter("x", emailAddress);
		req.setParameter("y", password);
		tx.commit();
		if (req != null)
			u = (User) req.getResultList().get(0);
		// if(u==null) throw new RuntimeException("Wrong login/pwd");
		return u;
	}

	public User addUser(User u) {
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		em.persist(u);
		tx.commit();
		return u;
	}
	
	public User deleteSession(User u) {
		String queryString = "update User set session = :sessionKey";

		EntityManager em = emf.createEntityManager();
		
		em.getTransaction().begin();
		Query query = em.createQuery(queryString);
		query.setParameter("sessionKey", null);
		query.executeUpdate();
		em.getTransaction().commit();
		
//		EntityTransaction tx = em.getTransaction();
//		u.setSession(null);
//		tx.begin();
//		em.merge(u);
//		tx.commit();
		return u;
	}

	public User getUserById(Long userId) {
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		User u = em.find(User.class, userId);
		if (u == null)
			throw new RuntimeException("user not found");
		tx.commit();
		return u;
	}

	public User signUp(String emailAddress, String password, String firstName,
			String lastName, String phoneNumber, boolean isGuide,
			Session session) {
		// create new entity manager
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();

		User u = null;
		System.out.println("je suis là ");

		if (isGuide) {
			u = new Guide(emailAddress, password, firstName, lastName,
					phoneNumber, null, null, null, null, false);
		} else {
			u = new User(emailAddress, password, firstName, lastName,
					phoneNumber);
		}
		
		session.setUser(u);
		u.setSession(session);

		tx.begin();
		// persist and commit
		em.persist(u);
		tx.commit();

		return u;
	}

	public User getUserByEmailAddress(String emailAddress) {
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		User u = null;
		tx.begin();
		Query query =
				em.createQuery("select u from User u where u.emailAddress=:x");
		query.setParameter("x", emailAddress);
		tx.commit();
		if (query != null && !query.getResultList().isEmpty())
			u = (User) query.getResultList().get(0);
		return u;
	}

	public User getUserBySessionKey(String sessionKey) {

		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		Session s = em.find(Session.class, sessionKey);
		tx.commit();
		User user = s.getUser();
		user.setSession(s);
		return user;
	}

	
	
	public Fingerprinting saveUserFingerprinting(Fingerprinting fp) {
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		em.persist(fp);
		tx.commit();
		return fp;
	}
}
