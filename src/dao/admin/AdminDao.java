package dao.admin;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import entities.Admin;




public class AdminDao {

	private static EntityManagerFactory emf =
			Persistence.createEntityManagerFactory("ProjetDAR");

	

	public Admin addAdmin(Admin a) {
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		em.persist(a);
		tx.commit();
		return a;
	}

	
	
}
