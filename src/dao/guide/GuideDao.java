package dao.guide;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import entities.Guide;
import entities.User;

public class GuideDao {

	private static EntityManagerFactory emf =
			Persistence.createEntityManagerFactory("ProjetDAR");
	EntityManager em = emf.createEntityManager();
	EntityTransaction tx = em.getTransaction();
	
	public Guide addGuide(Guide g) {
		tx.begin();
		em.persist(g);
		tx.commit();
		return g;
	}

	public Guide getGuideById(Long guideId) {
		tx.begin();
		Guide g = em.find(Guide.class, guideId);
		if(g==null) throw new RuntimeException("Guide not found");
		return g;
	}

	public List<Guide> getGuides() {
		tx.begin();
		Query req = em.createQuery("select g from Guide g");
		tx.commit();
		return req.getResultList();
	}

	public void approveGuide(Long guideId) {
		tx.begin();
		Guide g = em.find(Guide.class, guideId);
		g.setApprouved(true);
		em.refresh(g);
		
		
	}

	
	
}
