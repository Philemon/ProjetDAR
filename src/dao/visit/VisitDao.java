package dao.visit;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import entities.Guide;
import entities.Location;
import entities.User;
import entities.Visit;

public class VisitDao{

	// static entity manager factory
	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("ProjetDAR");

	public Visit addVisit(Visit v, Long guideId) {
		// create entity manager
		EntityManager em1 = emf.createEntityManager();
		// get transaction
		EntityTransaction tx = em1.getTransaction();
		Visit v1 = null;
		// begin transaction
		tx.begin();
		if (guideId != null) {

			// load guide from database
			Guide g = em1.find(Guide.class, guideId);
			// set guide
			v.setGuide(g);

			System.out.println(v.getGuide().getEmailAddress());
		}
		// create new entity manager
		EntityManager em2 = emf.createEntityManager();
		EntityTransaction tx2 = em2.getTransaction();
		// begin new transaction
		tx2.begin();

		v1 = em2.merge(v);
		em2.persist(v1);
		tx2.commit();
		return v1;
	}

	public void updateVisit(Visit v) {
		// create new entity manager
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		em.merge(v);
		tx.commit();

	}

	public void deleteVisit(Long visitId) {
		// create new entity manager
		Visit v;
		Visit v1;
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		em.remove(em.merge(em.find(Visit.class, visitId)));
		tx.commit();

	}
	
	public Visit editVisit(Long visitId) {
		// create new entity manager
		Visit v;
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		v = em.find(Visit.class, visitId);
		tx.commit();
		return v;
	}

	public List<Location> findVisitByLocation(String location) {
		// create new entity manager
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		List<Location> listLocations;
		tx.begin();
		Query req = em.createQuery("select l from Location l where l.location like :x");
		req.setParameter("x", "%" + location + "%");
		tx.commit();
		listLocations = req.getResultList();
		return listLocations;
	}

	public Visit findVisitById(Long visitId) {
		// create new entity manager
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		Visit v = em.find(Visit.class, visitId);
		if (v == null)
			throw new RuntimeException("Visit not found");
		// create new entity manager
		EntityManager em1 = emf.createEntityManager();
		// create new transaction
		EntityTransaction tx1 = em1.getTransaction();
		// begin new transaction
		tx1.begin();
		// persist and commit
		tx1.commit();
		return v;
	}

	public User participateInAVisit(Long userId, Long visitId) {
		// create new entity manager
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		User u = em.find(User.class, userId);
		Visit v = em.find(Visit.class, visitId);
		v.getUsers().add(u);
		u.getVisites().add(v);
		tx.commit();
		
		return u;
	}

	public void voteForAVisit(Long visitId) {
		// TODO Auto-generated method stub

	}

	public List<Visit> findAllVisits() {
		// create new entity manager
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		List<Visit> listVisits;

		tx.begin();
		Query req = em.createQuery("select v from Visit v");
		tx.commit();
		listVisits = req.getResultList();
		return listVisits;
	}

	public List<Visit> findAllVisitsForGuide(Long guideId) {
		// create new entity manager
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		List<Visit> listVisits;

		tx.begin();
		Query req = em.createQuery("select v from Visit v where v.guide.id=:x");
		req.setParameter("x", guideId);
		tx.commit();
		listVisits = req.getResultList();
		return listVisits;
	}

	public Location addLocation(Location locationn, Long visitId) {
		// create new entity manager
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		Visit v = em.find(Visit.class, visitId);
		locationn.setVisit(v);
		em.persist(locationn);
		tx.commit();
		return locationn;
	}

}
