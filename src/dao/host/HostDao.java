package dao.host;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import entities.Host;

public class HostDao {

	private static EntityManagerFactory entityManagerFactory =
			Persistence.createEntityManagerFactory("ProjetDAR");

	private EntityManager entityManager =
			entityManagerFactory.createEntityManager();

	public Host add(Host host) {
		this.entityManager.getTransaction().begin();
		this.entityManager.persist(host);
		this.entityManager.getTransaction().commit();
		
		return host;
	}
	
	@SuppressWarnings("unchecked")
	public List<Host> findByApiKey(String apiKey) {
		List<Host> hosts;
		
		String queryString = "select h from Host h where h.apiKey = :apiKey";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("apiKey", apiKey);
		
		hosts = query.getResultList();

		return hosts;
	}
	
	public boolean exists(String host) {
		String queryString = "select h from Host h where h.host = :host";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("host", host);
		
		return !query.getResultList().isEmpty();
	}

}
