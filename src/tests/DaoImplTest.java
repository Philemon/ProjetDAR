package tests;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import dao.admin.AdminDao;
import dao.coment.CommentDao;
import dao.guide.GuideDao;
import dao.user.UserDao;
import dao.visit.VisitDao;
import dao.visit_picture.VisitPictureDao;
import entities.User;

public class DaoImplTest {

	private AdminDao adminDao;
	private CommentDao commentDao;
	private GuideDao guideDao;
	private UserDao userDao;
	private VisitDao visitDao;
	private VisitPictureDao visitePicturedao;
	EntityManagerFactory emf;
	EntityManager em;

	@Before
	public void setUp() throws Exception {
		//commentDao = new CommentDao();
		guideDao = new GuideDao();
		userDao = new UserDao();
		visitDao = new VisitDao();
		visitePicturedao = new VisitPictureDao();
		emf = Persistence.createEntityManagerFactory("ProjetDAR");
		em = emf.createEntityManager();
	}

	@After
	public void tearDown() throws Exception {
		em.close();
		emf.close();
	}

	@Test
	public void test() {
		EntityTransaction tx = em.getTransaction();
		tx.begin();

		
		// visitDao.addVisit(new Visit("ffff", new Date(), "ttt"), 1L);
		//userDao.addUser(new User("so@gmail.com", "upmc91", "SOW	", "Abdoulaye", "06 80 46 78 45"));
		// visitDao.addVisit(new Visit("Champs Elysées", "54 Rue de l'Evangile,75018 Paris, France", new Date(), "cette visite porte sur.."), 2L);
		// userDao.addUser(new Guide("sow@gmail.com", "upmc91", "SOW",
		// "Abdoulaye", "07 53 59 43 76", null, null, "je suis..", null,
		// false));
		// visitDao.participateInAVisit(2l, 1l);
		// tx.commit();

		/*
		 * //Visite v = new Visite("visite à Paris",
		 * "la 1ère visite de la Tour Eiffel", new Date(), "Paris", "20°C");
		 * Guide g = new Guide("heng@gmail.com", "zeng91");
		 * g.setNomUser("heng"); g.setPrenomUser("zeng"); g.setAge(25);
		 * 
		 * //Admin a = new Admin("sowabdoulaye@gmail.com", "upmc91");
		 * //a.setNomUser("SOW"); //a.setPrenomUser("ABDOULAYE");
		 * //adminDao.addAdmin(a); //Visit v = metier.addVisite(new
		 * Visit("Houille", "Carriere sur seine", new Date(), "ville de Houille"
		 * )); //System.out.println("Nom :  "+ v.getDescription());
		 * 
		 * //EntityTransaction tx = em.getTransaction(); //tx.begin();
		 * 
		 * 
		 * 
		 * AdminDAO :ok Admin a = adminDao.addAdmin(new
		 * Admin("bouzyphil91@gmail.com", "upmcstlbouzy")); System.out.println(
		 * "login : "+a.getLogin()+" "+"Password : "+a.getPassword()); Admin a2
		 * = adminDao.addAdmin(new Admin("hengzeng@gmail.com", "upmcstlheng"));
		 * System.out.println("login : "+a2.getLogin()+" "+"Password : "
		 * +a2.getPassword());
		 * 
		 * 
		 * 
		 * UserDAO:ok
		 * 
		 * User u = userDao.addUser(new User("heng@gmail.com", "upmc92", "Zeng",
		 * "Heng", "07 52 59 50 76"));
		 * 
		 * System.out.println("login : "+u.getEmailAddress()+" "+"Password : "
		 * +u.getPassword() +"Fisrt Name : "+u.getFirstName()+" "+"Last Name : "
		 * +u.getLastName()+ " " +"Phone Number : "+u.getPhoneNumber()); User u1
		 * = userDao.getUserById(1L);
		 * 
		 * User u = userDao.signIn("abdoulayesow91@gmail.com", "upmc91");
		 * System.out.println("login : "+u.getEmailAddress()+" "+"Password : "
		 * +u.getPassword());
		 * 
		 * User u = userDao.getUserById(2L); System.out.println("login : "
		 * +u.getEmailAddress()+" "+"Password : "+u.getPassword());
		 * 
		 * 
		 * GuideDao :
		 * 
		 * Guide g1 = guideDao.addGuide(new Guide("sow@gmail.com", "upmc91",
		 * "SOW", "Abdoulaye", "07 52 59 50 76", null, "11/06/1991",
		 * "Ingenieur en ...",null,false)); Guide g2 = guideDao.addGuide(new
		 * Guide("philem@gmail.com", "upmc91", "Philemon", "Bouzy",
		 * "07 82 59 50 76", null, "11/06/1991", "Ingenieur en ..."
		 * ,null,false)); Guide g3 = guideDao.addGuide(new
		 * Guide("heng@gmail.com", "upmc91", "Heng", "ZENG", "06 52 59 50 76",
		 * null, "11/06/1991", "Ingenieur en ...",null,false));
		 * 
		 * Guide g = guideDao.addGuide(new Guide("sow91@gmail.com", "upmc",
		 * "SOW", "Abdoulaye", "06 80 16 57 68", null, "17/06/1991",
		 * "je suis ingénieur logiciel à l'université de ..", null, false));
		 * Guide g2 = guideDao.addGuide(new Guide("heng91@gmail.com", "upmc",
		 * "Heng", "Zeng", "06 80 16 59 68", null, "17/06/1992",
		 * "je suis ingénieur logiciel à l'université de lorens ..", null,
		 * false)); Guide g3 = guideDao.addGuide(new Guide("bouzy91@gmail.com",
		 * "upmc", "BOUZY", "Philemonn", "06 80 16 57 77", null, "17/06/1993",
		 * "je suis ingénieur logiciel à l'université de Paris decart ..", null,
		 * false)); System.out.println("login : "+g.getEmailAddress()+" "+
		 * "Password : "+g.getPassword() +"Fisrt Name : "+g.getFirstName()+" "+
		 * "Last Name : "+g.getLastName()+ " " +"Phone Number : "
		 * +g.getPhoneNumber()+"Birth Date : "+g.getBirthDate()+
		 * "Presentation : "+g.getPresentation());
		 * 
		 * Guide g1 = guideDao.getGuideById(4L); System.out.println("login : "
		 * +g.getEmailAddress()+" "+"Password "+g.getPassword()+" "+ "type : "
		 * +g.getClass().getSimpleName()+" "+"Presentation "
		 * +g.getPresentation());
		 * 
		 * 
		 * //guideDao.approveGuide(4L); //Guide g = guideDao.getGuideById(6L);
		 * //guideDao.approveGuide(4L); erreur //System.out.println(
		 * "FirstName : "+g.getFirstName());
		 * 
		 * //guideDao.approveGuide(6L);
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * VisiteDao :
		 * 
		 * 
		 * //visitDao.addVisit(new Visit("Pompidou", "Chatelet", new Date(),
		 * "description", "32°C"), null); List<Visit> listVisites =
		 * visitDao.findAllVisits(); for (Visit visit : listVisites) {
		 * System.out.println("Name : "+visit.getName()); }
		 * 
		 * 
		 * Visit visit = visitDao.findVisitById(12L); System.out.println(
		 * "Name : "+visit.getName());
		 * 
		 * List<Visit> listVisites = visitDao.findVisitByLocation("Chatelet");
		 * for (Visit visit : listVisites) { System.out.println("Name : "
		 * +visit.getName()); }
		 * 
		 * //visitDao.participateToVisit(1L, 12L);
		 * visitDao.participateToVisit(2L, 13L);
		 * 
		 * //visitDao.deleteVisit(12L); visitDao.addVisit(new Visit("UPMC",
		 * "JUSSIEU", new Date(), "description", "32°C"), null);
		 * visitDao.addVisit(new Visit("TOUR EIFFEL", "PARIS", new Date(),
		 * "description", "12°C"), null);
		 * 
		 * //je vais le revoir après //visitDao.updateVisit(new Visit("Havre",
		 * "JUSSIEU", new Date(), "description", "32°C")); //tx.commit();
		 * //commentDao.addComment(new Comment("Bliotheque Pompidou",
		 * "c'etait magnifique.. "), 13L, 4L);
		 * 
		 * //visitDao.addVisit(new Visit("Pompidou", "Chatelet", new Date(),
		 * "description", "32°C"), 4L);
		 * 
		 * 
		 * //Visit v1 = visitDao.addVisit(new Visit("Pompidou", "Chatelet", new
		 * Date(), "Pompidou est ....", "32°C"), 6L); //Visit v2 =
		 * visitDao.addVisit(new Visit("UPMC", "JUSSIEU", new Date(),
		 * "Upmc est la fac.....", "12°C"), 4L); //Visit v3 =
		 * visitDao.addVisit(new Visit("TOUR EIFFEL", "PARIS", new Date(),
		 * "tour eiffeil0.....", "14°C"), 5L);
		 * 
		 * List<Visit> listVisites = visitDao.findAllVisits(); for (Visit visit
		 * : listVisites) { System.out.println("Name : "+visit.getName()+" "
		 * +visit.getGuide().getFirstName()+ " "+visit.getGuide().getId()); }
		 * 
		 * 
		 * 
		 * Visit visit = visitDao.findVisitById(16L); System.out.println(
		 * "Name : "+visit.getName()+" "+visit.getGuide().getFirstName()+ " "
		 * +visit.getGuide().getId());
		 * 
		 * 
		 * List<Visit> listVisites = visitDao.findVisitByLocation("JUSSIEU");
		 * for (Visit visit : listVisites) { System.out.println("Name : "
		 * +visit.getName()+" "+visit.getGuide().getFirstName()+ " "
		 * +visit.getGuide().getId()); }
		 * 
		 * //visitDao.deleteVisit(14L);
		 * 
		 * Comment c = commentDao.addComment(new Comment("2er commentaire",
		 * "la visite etait.dddd."), 15L, 2L); System.out.println("mark : "
		 * +c.getMark()+ " "+c.getContent()+" "+c.getUser().getFirstName()+ " "
		 * +c.getVisit().getLocation()); Comment c2 = commentDao.addComment(new
		 * Comment("3er commentaire", "la visite etait.dszz."), 15L, 3L);
		 * System.out.println("mark : "+c2.getMark()+ " "+c2.getContent()+" "
		 * +c2.getUser().getFirstName()+ " "+c2.getVisit().getLocation());
		 * Comment c3 = commentDao.addComment(new Comment("4er commentaire",
		 * "la visite etait.ttt."), 15L, 4L); System.out.println("mark : "
		 * +c3.getMark()+ " "+c3.getContent()+" "+c3.getUser().getFirstName()+
		 * " "+c3.getVisit().getLocation());
		 * 
		 * Comment c3 =commentDao.getCommentById(6L);
		 * 
		 * System.out.println("mark : "+c3.getMark()+ " "+c3.getContent()+" "
		 * +c3.getUser().getFirstName()+ " "+c3.getVisit().getLocation());
		 * 
		 * 
		 * commentDao.deleteComment(3L);
		 * 
		 * //visitePicturedao.addVisitPicture(new VisitPicture(null), 15L);
		 * //visitePicturedao.deleteVisitPicture(2L); VisitPicture vp =
		 * visitePicturedao.getVisitPictureById(3L); System.out.println(
		 * "mark : "+vp.getVisit().getDescription()+ " "
		 * +vp.getVisit().getComments().size()+" "+vp.getVisit().getLocation());
		 * User u = userDao.addUser(new User("sow@gmail.com", "upmc91", "SOW",
		 * "Abdoulaye", "07 52 59 50 76")); //User u1 = new
		 * User("sow@gmail.com", "upmc91", "SOW", "Abdoulaye", "07 53 59 43 76"
		 * );
		 * 
		 * 
		 */

	}

}
