package entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.validator.constraints.NotEmpty;
import org.json.JSONException;
import org.json.JSONObject;

@Entity
public class Host implements Serializable {
	
	private static final long serialVersionUID = 2069007469977551146L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotEmpty
	private String host;
	
	@NotEmpty
	private String apiKey;
	
	public Host() {
	}
	
	public Host(String host, String apiKey) {
		this();
		this.setHost(host);
		this.setApiKey(apiKey);
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getHost() {
		return this.host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getApiKey() {
		return this.apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	
	public JSONObject toJSONObject() throws JSONException {
		JSONObject json = new JSONObject();
		
		json.accumulate("host", this.host);
		json.accumulate("apiKey", this.apiKey);
		
		return json;
	}

}
