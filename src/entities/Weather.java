package entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Weather implements Serializable {

	/**
	 */
	private static final long serialVersionUID = -4126202038032322343L;
	@Id
	private Date date;
	private int maxtemp;
	private int mintemp;
	private String typeTemp;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getMaxtemp() {
		return maxtemp;
	}

	public void setMaxtemp(int maxtemp) {
		this.maxtemp = maxtemp;
	}

	public int getMintemp() {
		return mintemp;
	}

	public void setMintemp(int mintemp) {
		this.mintemp = mintemp;
	}

	public String getTypeTemp() {
		return typeTemp;
	}

	public void setTypeTemp(String typeTemp) {
		this.typeTemp = typeTemp;
	}

	public Weather(Date date, int maxtemp, int mintemp, String typeTemp) {
		super();
		this.date = date;
		this.maxtemp = maxtemp;
		this.mintemp = mintemp;
		this.typeTemp = typeTemp;
	}

	public Weather() {
		super();
	}
}
