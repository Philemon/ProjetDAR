package entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class Fingerprinting implements Serializable {

private static final long serialVersionUID = -6351634102890361046L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String platform;
	
	
	private String screenResolution;
	
	
	private String userAgent;
	
	
	private String canvasFingerprinting;


	
	public Fingerprinting() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	


	public Fingerprinting(String platform, String screenResolution, String userAgent, String canvasFingerprinting) {
		super();
		this.platform = platform;
		this.screenResolution = screenResolution;
		this.userAgent = userAgent;
		this.canvasFingerprinting = canvasFingerprinting;
	}




	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getPlatform() {
		return platform;
	}


	public void setPlatform(String platform) {
		this.platform = platform;
	}


	public String getScreenResolution() {
		return screenResolution;
	}


	public void setScreenResolution(String screenResolution) {
		this.screenResolution = screenResolution;
	}


	public String getUserAgent() {
		return userAgent;
	}


	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}


	public String getCanvasFingerprinting() {
		return canvasFingerprinting;
	}


	public void setCanvasFingerprinting(String canvasFingerprinting) {
		this.canvasFingerprinting = canvasFingerprinting;
	}
	
	
	
}
