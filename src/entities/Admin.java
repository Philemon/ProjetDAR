package entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class Admin implements Serializable {

	private static final long serialVersionUID = -4126202038032322343L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotEmpty
	private String login;
	@NotEmpty
	private String password;
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "adminId")
	private Collection<Guide> guides;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Collection<Guide> getGuides() {
		return guides;
	}

	public void setGuides(Collection<Guide> guides) {
		this.guides = guides;
	}

	public Admin(String login, String password) {
		super();
		this.login = login;
		this.password = password;
	}

	public Admin() {
		super();
		// TODO Auto-generated constructor stub
	}
}
