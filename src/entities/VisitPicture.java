package entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class VisitPicture implements Serializable {

	private static final long serialVersionUID = -3331637465211573504L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String pictureURL;

	@ManyToOne
	@JoinColumn(name = "visitId")
	private Visit visit;

	public VisitPicture() {
		super();
	}

	public VisitPicture(String pictureURL) {
		super();
		this.pictureURL = pictureURL;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPicture() {
		return pictureURL;
	}

	public void setPicture(String pictureURL) {
		this.pictureURL = pictureURL;
	}

	public Visit getVisit() {
		return visit;
	}

	public void setVisit(Visit visit) {
		this.visit = visit;
	}

}
