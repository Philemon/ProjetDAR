package entities;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

@Entity
@DiscriminatorValue(value = "guide")
public class Guide extends User {

	private static final long serialVersionUID = 2741419758493041516L;

	private byte[] profilePicture;
	private String birthDate;
	private String presentation;
	private String paypalAccount;
	private boolean isApprouved;

	@OneToMany(mappedBy = "guide", fetch = FetchType.LAZY,
			cascade = CascadeType.ALL)
	private Collection<Visit> visits;

	public Guide() {
		super();
	}

	public Guide(String emailAddress, String password, String firstName,
			String lastName, String phoneNumber, byte[] profilePicture,
			String birthDate, String presentation, String paypalAccount,
			boolean isApprouved) {
		super(emailAddress, password, firstName, lastName, phoneNumber);
		this.profilePicture = profilePicture;
		this.birthDate = birthDate;
		this.presentation = presentation;
		this.paypalAccount = paypalAccount;
		this.isApprouved = isApprouved;
	}

	public byte[] getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(byte[] profilePicture) {
		this.profilePicture = profilePicture;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getPresentation() {
		return presentation;
	}

	public void setPresentation(String presentation) {
		this.presentation = presentation;
	}

	public String getPaypalAccount() {
		return paypalAccount;
	}

	public void setPaypalAccount(String paypalAccount) {
		this.paypalAccount = paypalAccount;
	}

	public boolean isApprouved() {
		return isApprouved;
	}

	public void setApprouved(boolean isApprouved) {
		this.isApprouved = isApprouved;
	}

	public Collection<Visit> getVisits() {
		return visits;
	}

	public void setVisits(Collection<Visit> visits) {
		this.visits = visits;
	}

}
