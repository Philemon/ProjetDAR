package services;

import org.json.JSONObject;

import dao.admin.AdminDao;
import entities.Admin;

public class AdminServices  {

	//create dao object
	AdminDao dao = new AdminDao();
	
	public JSONObject addAdmin(String login, String password) throws Exception {
		
		// call dao 
		Admin admin = dao.addAdmin(new Admin(login, password));
		// convert it to json
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("login", login);
		jsonObject.put("pwd", password);
		return jsonObject;
	}

	

	
}
