package services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Locale;

import javax.servlet.http.Part;

import org.json.JSONException;
import org.json.JSONObject;

import back_end.BackEndTask;
import dao.visit.VisitDao;
import entities.Guide;
import entities.Location;
import entities.User;
import entities.Visit;
import services.exceptions.ServiceException;
import services.util.ServicesTools;

public class VisitServices {

	private static final String NAME_PATTERN = "^[a-zA-Z0-9_-]{3,25}$";
	private static final String DATE_PATTERN = "dd-MM-yyyy";

	private static VisitDao dao = new VisitDao();

	public static JSONObject addVisit(String name, String location, String lng,
			String lat, String date, String description, String sessionKey,
			Collection<Part> picture, String appPath) throws JSONException {
		try {
			
			// Vérification des paramètres
			checkVisitParameters(name, location, date, description);
			
			// Création d'une visite
			Visit visit = new Visit(name,
					new SimpleDateFormat("dd-MM-yyyy").parse(date),
					description);
			
			// creation d'une location
			Location locationEntity = new Location(location, lat, lng);
			
			User u = UserServices.getUserBySessionKey(sessionKey);
			//if (!(u instanceof Guide))
				//throw new ServiceException(111, "Vous n'êtes pas un guide.");
			
			// ajout de la visite dans la Bd
			Visit v = dao.addVisit(visit, u.getId());
			if (v != null) {
				// Ajout de l'image dans la bd
				JSONObject json = VisitPictureServices.addVisitPicture(picture,
						appPath, String.valueOf(v.getId()));
				
				// Ajout de la location dans la bd
				Location l = dao.addLocation(locationEntity, v.getId());
				json.accumulate("location", l.getLocation());
				json.accumulate("lng", l.getLng());
				json.accumulate("lat", l.getLat());

				return json;
			}
		} catch (ServiceException e) {
			return ServicesTools.serviceRefused(e.getMessage(),
					e.getErrorCode());
		} catch (ParseException e) {
			// Ne se produira jamais
		}
		return null;
	}

	private static void checkVisitParameters(String name, String location,
			String date, String description) throws ServiceException {
		if (!name.matches(NAME_PATTERN))
			throw new ServiceException(111, "Nom invalide.");
		if (!isDateValid(date))
			throw new ServiceException(111, "Date invalide.");
	}

	private static boolean isDateValid(String date) {
		if (date == null)
			return false;

		try {
			new SimpleDateFormat(DATE_PATTERN, Locale.ENGLISH).parse(date);
			return true;
		} catch (ParseException e) {
			return false;
		}
	}

}
