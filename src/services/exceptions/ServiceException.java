package services.exceptions;

public class ServiceException extends Exception {

	private static final long serialVersionUID = 4088212714195893750L;

	private int errorCode;
	private String message;

	public ServiceException(int errorCode, String message) {
		this.errorCode = errorCode;
		this.message = message;
	}

	public int getErrorCode() {
		return this.errorCode;
	}

	public String getMessage() {
		return this.message;
	}

}
