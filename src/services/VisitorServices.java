package services;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;

import controllers.util.ServletsConfig;

import dao.visit.VisitDao;
import dao.visit_picture.VisitPictureDao;
import dao.weather.WeatherDao;
import entities.Location;
import entities.User;
import entities.Visit;
import entities.VisitPicture;
import entities.Weather;
import services.util.ServicesTools;

public class VisitorServices {

	private static VisitDao dao = new VisitDao();
	private static VisitPictureDao visitPictureDao = new VisitPictureDao();

	public static JSONObject searchVisits(String location) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public static JSONObject getVisit(String id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public static JSONObject getVisitLocation(String id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public static JSONObject getVisitWeather(String id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public static JSONObject getVisitPicture(String id) throws JSONException {
		try {
			JSONObject json = new JSONObject();
			VisitPicture visitPicture =
					visitPictureDao.getVisitPictureById(Long.parseLong(id));

			json.accumulate(ServletsConfig.VISIT_PICTURE,
					StringEscapeUtils.escapeHtml4(visitPicture.getPicture()));

			return json;
		} catch (IllegalArgumentException e) {
			return ServicesTools.serviceRefused("Visite inexistante.", 111);
		}
	}

	public static JSONObject participateInAVisit(String sessionKey,
			String visitId) throws Exception {
		JSONObject json = new JSONObject();
		User user = UserServices.getUserBySessionKey(sessionKey);
		user = dao.participateInAVisit(user.getId(), Long.parseLong(visitId));

		json.accumulate(ServletsConfig.SUCCESS, "ok");
		json.accumulate(ServletsConfig.EMAIL_ADDRESS, user.getEmailAddress());
		
		return json;
	}

	public static JSONObject voteForAVisit(String userId, String visitId)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public static JSONObject addVisit(String name, String location, Date date,
			String description, String weather, Long guideId) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public static JSONObject updateVisit(String name, String location,
			Date date, String description, String weather) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public static JSONObject deleteVisit(String visitId) throws JSONException {
		JSONObject json = new JSONObject();

		try {
			dao.deleteVisit(Long.parseLong(visitId));
			json.accumulate(ServletsConfig.SUCCESS, "ok");

			return json;
		} catch (NumberFormatException e) {
			return ServicesTools
					.serviceRefused("Identifiant de la visite invalide.", 334);
		}
	}

	public static JSONObject editVisit(String visitId) throws JSONException {
		JSONObject jsons = new JSONObject();

		try {
			Visit v = dao.editVisit(Long.parseLong(visitId));

			jsons.accumulate(ServletsConfig.ID, v.getId());
			jsons.accumulate(ServletsConfig.VISIT_NAME, v.getName());
			jsons.accumulate(ServletsConfig.VISIT_DESCRIPTION,
					v.getDescription());
			jsons.accumulate(ServletsConfig.VISIT_DATE, v.getDate());

			List<Location> locations = (List<Location>) v.getLocations();
			System.out.println("size :: " + v.getLocations().size());
			for (Location location : locations) {
				JSONObject json = new JSONObject();
				json.accumulate(ServletsConfig.VISIT_LOCATION,
						location.getLocation());
				json.accumulate(ServletsConfig.LOCATION_LAT, location.getLat());
				json.accumulate(ServletsConfig.LOCATION_LNG, location.getLng());
				System.out.println(
						location.getLocation() + " " + location.getLat());
				json.append("visit", json);
			}

			return jsons;
		} catch (NumberFormatException e) {
			return ServicesTools
					.serviceRefused("Identifiant de la visite invalide.", 334);
		}
	}

	public static JSONObject findAllVisits() throws Exception {
		List<Visit> allVisits = dao.findAllVisits();
		JSONObject jsons = new JSONObject();
		JSONObject json = null;
		for (Visit visit : allVisits) {
			json = new JSONObject();
			json.accumulate(ServletsConfig.ID, String.valueOf(visit.getId()));
			json.accumulate(ServletsConfig.VISIT_NAME, visit.getName());
			// json.accumulate(ServletsConfig.VISIT_LOCATION,
			// visit.getLocation());
			json.accumulate(ServletsConfig.VISIT_DATE, visit.getDate());
			json.accumulate(ServletsConfig.VISIT_DESCRIPTION,
					visit.getDescription());
			// json.accumulate(ServletsConfig.VISIT_WEATHER,
			// visit.getWeather());
			// json.accumulate(ServletsConfig.VISIT_PICTURE,
			// visit.getPictures());
			jsons.accumulate("visit", json);
		}
		return jsons;
	}

	public static JSONObject findAllVisitsForGuide(String sessionKey)
			throws JSONException {
		try {
			User guide = UserServices.getUserBySessionKey(sessionKey);
			List<Visit> visits = dao.findAllVisitsForGuide(guide.getId());
			JSONObject json = new JSONObject();

			WeatherDao weatherDao = new WeatherDao();
			for (Visit visit : visits) {
				JSONObject visitJson = new JSONObject();
				visitJson.accumulate(ServletsConfig.ID, visit.getId());
				visitJson.accumulate(ServletsConfig.VISIT_NAME,
						visit.getName());
				visitJson.accumulate(ServletsConfig.VISIT_DATE,
						visit.getDate());

				Weather weather = weatherDao.getWeatherByDate(visit.getDate());

				if (weather != null) {

					visitJson.accumulate(ServletsConfig.TYPE_TEMP,
							weather.getTypeTemp());

					visitJson.accumulate(ServletsConfig.MIN_TEMP,
							weather.getMintemp());

					visitJson.accumulate(ServletsConfig.MAX_TEMP,
							weather.getMaxtemp());

				}
				visitJson.accumulate(ServletsConfig.VISIT_DESCRIPTION,
						visit.getDescription());
				// visitJson.accumulate(ServletsConfig.VISIT_WEATHER,
				// visit.getWeather());

				for (Location location : visit.getLocations()) {
					JSONObject locationJson = new JSONObject();
					locationJson.accumulate(ServletsConfig.LOCATION_NAME,
							location.getLocation());
					locationJson.accumulate(ServletsConfig.LOCATION_LAT,
							location.getLat());
					locationJson.accumulate(ServletsConfig.LOCATION_LNG,
							location.getLng());
					visitJson.append(ServletsConfig.VISIT_LOCATION,
							locationJson);
				}

				for (VisitPicture visitPicture : visit.getPictures())
					visitJson.append(ServletsConfig.VISIT_PICTURE,
							visitPicture.getPicture());

				json.append(ServletsConfig.VISIT_LIST, visitJson);
			}

			json.accumulate(ServletsConfig.VISIT_NUMBER, visits.size());

			return json;
		} catch (NumberFormatException e) {
			return ServicesTools.serviceRefused("Guide inexistant.", 119);
		}

	}

	public static JSONObject findAllParticipantsForVisit(String sessionKey)
			throws JSONException {
		try {
			JSONObject jsons = new JSONObject();
			User user = UserServices.getUserBySessionKey(sessionKey);
			List<Visit> listOfVisits = dao.findAllVisitsForGuide(user.getId());

			if (listOfVisits.size() != 0) {

				for (Visit v : listOfVisits) {

					List<User> users = (List<User>) v.getUsers();

					for (User u : users) {
						JSONObject json = new JSONObject();
						json.accumulate(ServletsConfig.VISIT_NAME, v.getName());
						json.accumulate(ServletsConfig.FIRST_NAME,
								user.getFirstName());
						json.accumulate(ServletsConfig.LAST_NAME,
								user.getLastName());
						json.accumulate(ServletsConfig.EMAIL_ADDRESS,
								user.getEmailAddress());
						jsons.accumulate("participant", json);
					}
				}

			}

			return jsons;
		} catch (NumberFormatException e) {
			return ServicesTools.serviceRefused("Visite inexistante.", 113);
		}
	}

	public static JSONObject getLocation(String q) throws Exception {
		// https://maps.googleapis.com/maps/api/place/textsearch/json?query=...
		// &key=AIzaSyCGtUqquCBXYvVCuriyx26GBih4dTghny4
		String api =
				"https://maps.googleapis.com/maps/api/place/textsearch/json?query="
						+ q + "&location=48.857650,2.339280&radius=13000&key="
						+ "AIzaSyCGtUqquCBXYvVCuriyx26GBih4dTghny4";
		System.out.println(q);
		JSONObject json = null;

		try {
			URL url = new URL(api);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException(
						"Failed : HTTP error code : " + conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(
					new InputStreamReader((conn.getInputStream())));

			StringBuilder sb = new StringBuilder();
			br.lines().forEach(sb::append);
			json = new JSONObject(sb.toString());
			System.out.println("Output from Server .... \n");

			conn.disconnect();

		} catch (MalformedURLException e) {

			e.printStackTrace();

		}

		return json;
	}

	public static JSONObject findVisitByLocation(String location)
			throws Exception {
		List<Location> listLocations = dao.findVisitByLocation(location);
		System.out.println("la taille  " + listLocations.size() + " "
				+ "location : " + location);
		WeatherDao weatherDao = new WeatherDao();
		JSONObject jsons = new JSONObject();
		JSONObject json = null;
		for (Location loc : listLocations) {
			json = new JSONObject();
			json.accumulate(ServletsConfig.LOCATION_NAME, loc.getLocation());
			json.accumulate(ServletsConfig.LOCATION_LNG, loc.getLng());
			json.accumulate(ServletsConfig.LOCATION_LAT, loc.getLat());

			Weather weather =
					weatherDao.getWeatherByDate(loc.getVisit().getDate());

			if (weather != null) {

				json.accumulate(ServletsConfig.TYPE_TEMP,
						weather.getTypeTemp());

				json.accumulate(ServletsConfig.MIN_TEMP, weather.getMintemp());

				json.accumulate(ServletsConfig.MAX_TEMP, weather.getMaxtemp());

			}

			json.accumulate(ServletsConfig.ID,
					String.valueOf(loc.getVisit().getId()));
			json.accumulate(ServletsConfig.VISIT_NAME,
					loc.getVisit().getName());
			// json.accumulate(ServletsConfig.VISIT_LOCATION,
			// visit.getLocation());
			json.accumulate(ServletsConfig.VISIT_DATE,
					loc.getVisit().getDate());

			json.accumulate(ServletsConfig.VISIT_DESCRIPTION,
					loc.getVisit().getDescription());
			// System.out.println("location : "+visit.getLocations());
			// json.accumulate(ServletsConfig.VISIT_WEATHER,
			// loc.getVisit().get);
			// json.accumulate(ServletsConfig.VISIT_PICTURE,
			// visit.getPictures());
			jsons.accumulate("visit", json);
		}
		return jsons;
	}

}
