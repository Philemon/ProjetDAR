package services.util;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * The {@code ServicesTools} class provide methods to create an error JSON or a
 * success JSON. It cannot be instantiated.
 * 
 * @author Philémon Bouzy
 * @version 1.0
 */
public class ServicesTools {

	/**
	 * The error code label.
	 */
	public static final String ERROR_CODE_LABEL = "errorCode";

	/**
	 * The error message label.
	 */
	public static final String ERROR_MESSAGE_LABEL = "errorMessage";

	/**
	 * Private constructor.
	 */
	private ServicesTools() {
	}

	/**
	 * Returns a {@code JSONObject} containing an error.
	 * 
	 * @param message
	 *            the error message
	 * @param errorCode
	 *            the error code
	 * @return a {@code JSONObject}
	 */
	public static JSONObject serviceRefused(String message, int errorCode) {
		JSONObject jsonError = new JSONObject();

		try {
			jsonError.accumulate(ERROR_MESSAGE_LABEL, message);
			jsonError.accumulate(ERROR_CODE_LABEL, errorCode);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return jsonError;
	}

	/**
	 * Returns an empty {@code JSONObject} in order to indicate that the service
	 * was accepted.
	 * 
	 * @return an empty {@code JSONObject}
	 */
	public static JSONObject serviceAccepted() {
		return new JSONObject();
	}

}
