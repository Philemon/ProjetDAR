package services.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

public class Javascript {

	private String script;

	private Map<String, String> elements;

	public Javascript(String script) {
		this.script = script;
		this.elements = new HashMap<>();
	}

	public Javascript(Path path) throws IOException {
		this(StringUtils.join(Files.readAllLines(path), '\n'));
	}

	public void addElement(String name, String element) {
		this.elements.put(name, element);
	}
	
	public String getElement(String name) {
		return this.elements.get(name);
	}

	@Override
	public String toString() {
		String string = this.script;

		for (String elementName : this.elements.keySet()) {
			string = string.replaceAll("§" + elementName + "§",
					this.elements.get(elementName));
		}

		return string;
	}

}
