package services.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class JavascriptTest {

	public static void main(String[] args) throws IOException {
		Path p = Paths.get("src/services/util/get-csrf-token.js");

		List<String> f = Files.readAllLines(p);
		
		String s = StringUtils.join(f, '\n');
		
		System.out.println(s);
	}

}
