var apiConfig = apiConfig || {};

if (§domainCondition§) {
	apiConfig.csrfToken = '§csrfToken§';

	// Invoke a callback if the partner wants us to
	if (typeof apiConfig.fnInit !== 'undefined') {
		apiConfig.fnInit();
	}
} else {
	alert('This site is not authorised for this API key.');
}