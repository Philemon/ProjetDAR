package services.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import entities.Host;

public class HostsConditionList {

	private List<String> hostsConditionList;

	public HostsConditionList() {
		this.hostsConditionList = new ArrayList<>();
	}

	public void addHost(Host host) {
		String domain = host.getHost();

		this.hostsConditionList.add("document.domain === '" + domain + "'");
		this.hostsConditionList.add("document.domain === 'www." + domain + "'");
	}
	
	@Override
	public String toString() {
		return StringUtils.join(this.hostsConditionList, " || ");
	}

}
