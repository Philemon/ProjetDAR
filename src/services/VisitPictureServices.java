package services;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import javax.servlet.http.Part;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;

import controllers.util.ServletsConfig;
import dao.visit_picture.VisitPictureDao;
import entities.VisitPicture;
import services.util.ServicesTools;

public class VisitPictureServices {

	private static final String SAVE_DIR = "uploads";

	private static VisitPictureDao dao = new VisitPictureDao();

	public static JSONObject addVisitPicture(Collection<Part> picture,
			String appPath, String visitId) throws JSONException {
		try {
			String savePath = appPath + File.separator + SAVE_DIR;
			String filePath = new String();
			
			System.out.println("savePath :: "+savePath);
			// creates the save directory if it does not exists
			File fileSaveDir = new File(savePath);
			if (!fileSaveDir.exists()) {
				fileSaveDir.mkdir();
			}

			for (Part part : picture) {
				String fileName = extractFileName(part);
				filePath = savePath + File.separator + "paris.jpg";
				part.write(filePath);
				System.out.println(filePath);
			}

			

			JSONObject json = new JSONObject();
			dao.addVisitPicture(new VisitPicture(filePath),
					Long.parseLong(visitId));

			json.accumulate(ServletsConfig.VISIT_PICTURE,
					StringEscapeUtils.escapeHtml4(filePath));

			return json;
		} catch (IllegalArgumentException e) {
			return ServicesTools.serviceRefused("Visite inexistante.", 111);
		} catch (IOException e) {
			System.out.println("error ::: "+e.getMessage());
			return ServicesTools.serviceRefused(
					"Erreur lors de l'hébergement de l'image.", 111);
		}
	}

	public static JSONObject getVisitPictureById(String id)
			throws JSONException {
		try {
			JSONObject json = new JSONObject();
			VisitPicture visitPicture =
					dao.getVisitPictureById(Long.parseLong(id));

			json.accumulate(ServletsConfig.VISIT_PICTURE,
					StringEscapeUtils.escapeHtml4(visitPicture.getPicture()));

			return json;
		} catch (IllegalArgumentException e) {
			return ServicesTools.serviceRefused("Visite inexistante.", 111);
		}
	}

	private static String extractFileName(Part part) {
		String contentDisp = part.getHeader("content-disposition");
		String[] items = contentDisp.split(";");
		for (String s : items) {
			if (s.trim().startsWith("filename")) {
				return s.substring(s.indexOf("=") + 2, s.length() - 1);
			}
		}
		return "";
	}

}
