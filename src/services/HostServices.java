package services;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONException;
import org.json.JSONObject;

import dao.host.HostDao;
import entities.Host;
import services.util.HostsConditionList;
import services.util.Javascript;
import services.util.ServicesTools;

public class HostServices {

	private static KeyPair keyPair = null;

	private static Encoder base64Encoder = Base64.getEncoder();

	private static Decoder base64Decoder = Base64.getDecoder();

	private static HostDao dao = new HostDao();

	public static JSONObject register(String host) throws JSONException {
		if (dao.exists(host)) {
			return ServicesTools.serviceRefused("Host already registered", 1);
		}

		String apiKey = createApiKey();

		return dao.add(new Host(host, apiKey)).toJSONObject();
	}

	public static Pair<Javascript, String> getCsrfToken(String apiKey,
			String secureRandom) throws Exception {
		SecureRandom randomGenerator = SecureRandom.getInstance("SHA1PRNG");

		if (keyPair == null) {
			keyPair = generateKeyPair();
		}

		if (secureRandom == null) {
			secureRandom = Long.toString(randomGenerator.nextLong());
		}

		// System.out.println(System.getProperty("user.dir"));

		Javascript javascript =
				new Javascript(Paths.get("js/get-csrf-token.js"));
		List<Host> allowedHosts = dao.findByApiKey(apiKey);
		HostsConditionList hostsConditionList = new HostsConditionList();

		allowedHosts.stream().forEach(hostsConditionList::addHost);

		String domainCondition = hostsConditionList.toString();
		byte[] token =
				ArrayUtils.addAll(apiKey.getBytes(), secureRandom.getBytes());
		String signature = new String(signToken(token, keyPair.getPrivate()));
		String signedToken = apiKey + ", " + secureRandom + ", " + signature;
		signedToken = base64Encoder.encodeToString(signedToken.getBytes());

		javascript.addElement("domainCondition", domainCondition);
		javascript.addElement("csrfToken", signedToken);

		return Pair.of(javascript, secureRandom);
	}

	public static boolean isTokenValid(String base64SignedToken) {
		String signedToken =
				new String(base64Decoder.decode(base64SignedToken));
		System.out.println("Signed Token = " + signedToken);
		String[] tokenArray = signedToken.split(", ");

		try {
			byte[] token = ArrayUtils.addAll(tokenArray[0].getBytes(),
					tokenArray[1].getBytes());

			return verifySignature(token, keyPair.getPublic(),
					tokenArray[2].getBytes());
		} catch (Exception e) {
			return false;
		}
	}

	private static byte[] signToken(byte[] token, PrivateKey key)
			throws Exception {
		Signature signer = Signature.getInstance("SHA256withRSA");
		signer.initSign(key);
		signer.update(token);

		return signer.sign();
	}

	private static boolean verifySignature(byte[] token, PublicKey key,
			byte[] signature) throws Exception {
		Signature signer = Signature.getInstance("SHA256withRSA");
		signer.initVerify(key);
		signer.update(token);

		return signer.verify(signature);
	}

	private static KeyPair generateKeyPair() throws Exception {
		KeyPairGenerator keyGenerator = KeyPairGenerator.getInstance("RSA");
		keyGenerator.initialize(1024);

		return keyGenerator.generateKeyPair();
	}

	private static String createApiKey() {
		return UUID.randomUUID().toString();
	}

}
