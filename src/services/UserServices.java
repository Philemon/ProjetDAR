package services;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Properties;
import java.util.UUID;

import org.json.JSONException;
import org.json.JSONObject;

import controllers.util.ServletsConfig;
import dao.user.SessionDao;
import dao.user.UserDao;
import entities.Fingerprinting;
import entities.Session;
import entities.User;
import services.exceptions.ServiceException;
import services.util.ServicesTools;

public class UserServices {

	private static final String USER_TYPE_LABEL = "type";

	private static final String FIRST_NAME_LABEL = "firstName";

	private static final String LAST_NAME_LABEL = "lastName";

	private static final String SESSION_KEY_LABEL = "sessionKey";

	private static final String EMAIL_ADDRESS_LABEL = "emailAddress";

	private static final String PASSWORD_PATTERN =
			"^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).{6,20}$";

	private static final String EMAIL_ADDRESS_PATTERN =
			"^[\\w-]+(?:\\.[\\w-]+)*@(?:[\\w-]+\\.)+[a-zA-Z]{2,7}$";

	private static final String NAME_PATTERN =
			"^(?:[\\p{L}\\p{Mn}\\p{Pd}\\'\\x{2019}]\\ ?)+$";

	private static final String PHONE_NUMBER_PATTERN =
			"^(0|\\+33)[1-9]([-. ]?[0-9]{2}){4}$";

	private static final long KEY_VALIDITY_DURATION = 120 * 60 * 1000;

	private static UserDao dao = new UserDao();

	private static SessionDao sessionDao = new SessionDao();

	/**
	 * Connecte un utilisateur.
	 * 
	 * @param emailAddress
	 *            adresse email
	 * @param password
	 *            mot de passe
	 * @return un JSONObject
	 * @throws JSONException
	 */
	public static JSONObject signIn(String emailAddress, String password)
			throws JSONException {
		// création d'un objet json
		JSONObject json = new JSONObject();
		User user = null;
		Session session = null;
		try {
			checkEmailAddressAndPassword(emailAddress, password);

			if (validSessionKeyExists(emailAddress)) {
				throw new ServiceException(200, "Vous êtes déjà connecté.");
			}
			
			user = getUserByEmailAddress(emailAddress);
			session = new Session(createSessionKey(),
					new Timestamp(new Date().getTime()));
			session.setUser(user);
			user.setSession(session);
			sessionDao.addSession(session);

			json.accumulate(USER_TYPE_LABEL, user.getClass().getSimpleName());
			json.accumulate(ServletsConfig.ID, user.getId());
			json.accumulate(FIRST_NAME_LABEL, user.getFirstName());
			json.accumulate(LAST_NAME_LABEL, user.getLastName());
			json.accumulate(SESSION_KEY_LABEL, session.getSessionKey());
		} catch (ServiceException e) {
			return ServicesTools.serviceRefused(e.getMessage(),
					e.getErrorCode());
		}

		return json;
	}

	/**
	 * Crée un nouveau compte utilisateur.
	 * 
	 * @param emailAddress
	 *            adresse email
	 * @param password
	 *            mot de passe
	 * @param firstName
	 *            prénom
	 * @param lastName
	 *            nom de famille
	 * @param phoneNumber
	 *            numéro de téléphone
	 * @param isGuide
	 *            type d'utilisateur (guide ou utilisateur classique)
	 * @return un JSONObject
	 * @throws JSONException
	 */
	public static JSONObject signUp(String emailAddress, String password,
			String firstName, String lastName, String phoneNumber,
			String isGuide) throws JSONException {
		JSONObject json = new JSONObject();

		try {
			checkUserBasicInformation(emailAddress, password, firstName,
					lastName, phoneNumber);

			Session session = new Session(createSessionKey(),
					new Timestamp(new Date().getTime()));
			User user = dao.signUp(emailAddress, password, firstName, lastName,
					phoneNumber, isGuide != null, session);

			if (user != null) {
				String userType = user.getClass().getSimpleName();
				json.accumulate(USER_TYPE_LABEL, userType);
				json.accumulate(FIRST_NAME_LABEL, user.getFirstName());
				json.accumulate(LAST_NAME_LABEL, user.getLastName());
				json.accumulate(SESSION_KEY_LABEL, session.getSessionKey());
				json.accumulate(ServletsConfig.ID, user.getId());
			}

			return json;
		} catch (ServiceException e) {
			return ServicesTools.serviceRefused(e.getMessage(),
					e.getErrorCode());
		}
	}

	/**
	 * Déconnecte un utilisateur.
	 * 
	 * @param sessionKey
	 *            clé de session
	 * @return un JSONObject
	 * @throws JSONException
	 */
	public static JSONObject signOut(String sessionKey) throws JSONException {
		User user = getUserBySessionKey(sessionKey);

		if (user == null)
			return ServicesTools.serviceRefused("Vous n'êtes pas connecté.",
					300);

		dao.deleteSession(user);
		// sessionDao.deleteSession(sessionKey);

		return ServicesTools.serviceAccepted();
	}

	public static JSONObject getUserInfo(String sessionKey)
			throws JSONException {
		JSONObject json = new JSONObject();

		if (sessionKey == null || sessionKey.equals("null")) {
			return ServicesTools.serviceRefused("Erreur.", 1);
		}

		User user = getUserBySessionKey(sessionKey);

		if (user == null) {
			return ServicesTools.serviceRefused("Vous n'êtes pas connecté.",
					300);
		}

		json.accumulate(EMAIL_ADDRESS_LABEL, user.getEmailAddress());
		json.accumulate(FIRST_NAME_LABEL, user.getFirstName());
		json.accumulate(LAST_NAME_LABEL, user.getLastName());

		return json;
	}

	protected static User getUserBySessionKey(String sessionKey) {
		Session session = sessionDao.getSessionBySessionKey(sessionKey);

		if (session == null)
			return null;

		return session.getUser();
	}

	private static void checkEmailAddressAndPassword(String emailAddress,
			String password) throws ServiceException {
		if (!isEmailAddressTaken(emailAddress))
			throw new ServiceException(7, "Adresse email incorrecte.");
		if (!isPasswordValid(emailAddress, password))
			throw new ServiceException(8, "Mot de passe incorrect.");
	}

	private static void checkUserBasicInformation(String emailAddress,
			String password, String firstName, String lastName,
			String phoneNumber) throws ServiceException {
		if (!isEmailAddressValid(emailAddress))
			throw new ServiceException(1, "Adresse email incorrecte.");
		if (isEmailAddressTaken(emailAddress))
			throw new ServiceException(2, "Adresse email déjà utilisée.");
		if (!isPasswordValid(password))
			throw new ServiceException(3, "Mot de passe invalide.");
		// if (!isNameValid(firstName))
		// throw new ServiceException(4, "Prénom incorrect.");
		if (!isNameValid(lastName))
			throw new ServiceException(5, "Nom de famille incorrect.");
		if (!isPhoneNumberValid(phoneNumber))
			throw new ServiceException(6, "Numéro de téléphone incorrect.");
	}

	private static boolean isPasswordValid(String password) {
		return password != null && password.matches(PASSWORD_PATTERN);
	}

	private static boolean isPasswordValid(String emailAddress,
			String password) {
		User user = getUserByEmailAddress(emailAddress);

		return user != null && user.getPassword().equals(password);
	}

	private static boolean isEmailAddressValid(String emailAddress) {
		return emailAddress != null
				&& emailAddress.matches(EMAIL_ADDRESS_PATTERN);
	}

	private static boolean isEmailAddressTaken(String emailAddress) {
		return getUserByEmailAddress(emailAddress) != null;
	}

	private static boolean isNameValid(String name) {
		return name != null && name.matches(NAME_PATTERN);
	}

	private static boolean isPhoneNumberValid(String phoneNumber) {
		return phoneNumber != null && phoneNumber.matches(PHONE_NUMBER_PATTERN);
	}

	private static User getUserByEmailAddress(String emailAddress) {
		return dao.getUserByEmailAddress(emailAddress);
	}

	private static String createSessionKey() {
		return UUID.randomUUID().toString();
	}

	private static boolean validSessionKeyExists(String emailAddress) {
		try {
			return getSessionKey(emailAddress) != null;
		} catch (Exception e) {
			return false;
		}
	}

	private static String getSessionKey(String emailAddress) {
		User user = getUserByEmailAddress(emailAddress);

		if (user == null || user.getSession() == null)
			return null;

		Timestamp timestamp = new Timestamp(
				new java.util.Date().getTime() - KEY_VALIDITY_DURATION);

		if (user.getSession().getCreated().after(timestamp))
			return user.getSession().getSessionKey();

		sessionDao.deleteSession(user.getSession());

		return null;
	}

	public static JSONObject acceptUser(String from, String email,
			String password) {

		String host = "smtp.gmail.com";
		Properties props = System.getProperties();
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.user", from);
		props.put("mail.smtp.password", password);
		props.put("mail.smtp.port", 587);
		props.put("mail.smtp.auth", 587);
		props.put("mail.smtp.port", "true");

		return null;
	}

	public static JSONObject saveUserFingerprinting(String platform,
			String resolution, String userAgent, String canvasFingerprinting)
			throws Exception {
		JSONObject json = new JSONObject();

		Fingerprinting fpinfos = new Fingerprinting(platform, resolution,
				userAgent, canvasFingerprinting);
		Fingerprinting fp = dao.saveUserFingerprinting(fpinfos);

		json.accumulate(ServletsConfig.SUCCESS, "ok");
		json.accumulate(ServletsConfig.PLATFORM, fp.getPlatform());
		json.accumulate(ServletsConfig.RESOLUTION, fp.getScreenResolution());
		json.accumulate(ServletsConfig.USER_AGENT, fp.getUserAgent());
		json.accumulate(ServletsConfig.CANVAS_FINGERPRINTING,
				fp.getCanvasFingerprinting());
		return json;
	}

}
