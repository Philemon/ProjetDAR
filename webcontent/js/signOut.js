function signOutFunction() {
	sessionKey = getCookie('sessionKey');

	// Envoi de la requête HTTP en mode asynchrone
	$.ajax({
		url : 'signout?sessionKey=' + sessionKey,
		type : 'GET',
		dataType : 'json', // JSON
		success : function (json) {
			setCookie('sessionKey', null);
			window.location.href = 'accueil.html';
		}
	});
}
