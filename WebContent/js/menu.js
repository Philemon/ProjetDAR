$(document).ready(function () {

	var signUp = '<li><a href="signup.html">Inscription</a></li>';
	var signIn = '<li><a href="#exampleModal" data-toggle="modal">Connexion</a></li>';
	var signOut = '<li><a href="#" onclick="signOutFunction(); return true;">Déconnexion</a></li>';
	
	var sessionKey = getCookie('sessionKey');
	
	console.log(sessionKey);
	
	if (sessionKey == null) {
		$('#menu-ul').append(signUp);
		$('#menu-ul').append(signIn);
		return;
	}

	var url = 'user?sessionKey=' + sessionKey;
	
	$.getJSON(url).done(function (user) {
		if (user.errorCode != undefined) {
			$('#menu-ul').append(signUp);
			$('#menu-ul').append(signIn);
			setCookie('sessionKey', null);
		} else {
			var message = '<li><a href="#" style="cursor: text; font-weight: normal;">Bienvenue ' + user.firstName + '</a></li>';
			
			$('#menu-ul').append(message);
			$('#menu-ul').append(signOut);
		}
	});
	
});