// script pour le signUp

$(document).ready(function() {
    // Lorsque je soumets le formulaire
     $('#signUp').on('submit', function(e) {
        e.preventDefault(); // J'empêche le comportement par défaut du navigateur, c-à-d de soumettre le formulaire

        var $this = $(this); // L'objet jQuery du formulaire

        // Je récupère les valeurs
        var password = $('#password').val();
        var confirmePassword = $('#confirmePassword').val();

        // Je vérifie
        if(password != confirmePassword ) {
        	$("#alert_div").show();
            $("#message_erreur").html("password non identique");
        } else {
            // Envoi de la requête HTTP en mode asynchrone
            $.ajax({
                url: 'signup', // Le nom du controlleur à appeler
                type: $this.attr('method'), // La méthode indiquée dans le formulaire (get ou post)
                data: $this.serialize(), // Je sérialise les données (j'envoie toutes les valeurs présentes dans le formulaire)
                dataType: 'json', // type du retour : JSON
                success: function(json) {
                 console.log("type : "+json.type);
                 setCookie("idUser",json.id);
                 setCookie("sessionKey",json.sessionKey);
                 setCookie("type",json.type);
                 setCookie("firstName",json.firstName);
                 setCookie("lastName",json.lastName);
                 if (json.type == "Guide") window.location.href = 'guideSpace.html';
                 else window.location.href = 'accueil.html';
                }
            });
        }
    });
});
