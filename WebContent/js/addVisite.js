$(document).ready(function() {
    // Lorsque je soumets le formulaire
    $('#monForm').on('submit', function(e) {
        e.preventDefault(); // J'empêche le comportement par défaut du navigateur, c-à-d de soumettre le formulaire
 
        var $this = $(this); // L'objet jQuery du formulaire
      //les données du formulaire
        var formData = new FormData($(this)[0]);
 
            // Envoi de la requête HTTP en mode asynchrone
            $.ajax({
                url: $this.attr('action'), // Le nom du controller
                type: $this.attr('method'), // La méthode indiquée dans le formulaire (get ou post)
                data: formData, //$this.serialize(), // Je sérialise les données (j'envoie toutes les valeurs présentes dans le formulaire)
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'json', // JSON
                success: function(json) {
                	
                		console.log(json);
                		$("#visiteAjouter").show();
                		location.reload();
                		//$("#visiteAjouter").append("<img src=data:"+json.file+" alt=\"visite image\" />");
                }
            });
        
    });
});