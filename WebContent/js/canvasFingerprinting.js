$(document).ready(function () {

	var fp1 = new Fingerprint();
	var fp2 = new Fingerprint({
		canvas : true
	});
	var fp3 = new Fingerprint({
		ie_activex : true
	});
	var fp4 = new Fingerprint({
		screen_resolution : true
	});

	var platform = window.navigator.platform;
	var cookieEnabled = window.navigator.cookieEnabled ? "yes" : "no";
	var timezone = new Date().getTimezoneOffset();
	var resolution = window.screen.width + "x"
			+ window.screen.height + "x"
			+ window.screen.colorDepth;
	var userAgent = navigator.userAgent;

	try {
		localStorage.fp = "test";
		domLocalStorage = "";
		if (localStorage.fp == "test") {
			domLocalStorage = "yes";
		} else {
			domLocalStorage = "no";
		}
	} catch (ex) {
		domLocalStorage = "no";
	}

	try {
		canvas = document.createElement("canvas");
		canvas.height = 60;
		canvas.width = 400;
		canvasContext = canvas.getContext("2d");
		canvas.style.display = "inline";
		canvasContext.textBaseline = "alphabetic";
		canvasContext.fillStyle = "#f60";
		canvasContext.fillRect(125, 1, 62, 20);
		canvasContext.fillStyle = "#069";
		canvasContext.font = "11pt no-real-font-123";
		canvasContext.fillText(
				"Cwm fjordbank glyphs vext quiz, \ud83d\ude03",
				2, 15);
		canvasContext.fillStyle = "rgba(102, 204, 0, 0.7)";
		canvasContext.font = "18pt Arial";
		canvasContext.fillText(
				"Cwm fjordbank glyphs vext quiz, \ud83d\ude03",
				4, 45);
		canvasData = canvas.toDataURL();
	} catch (e) {
		canvasData = "Not supported";
	}

	var canvas = document.createElement('canvas');
	var ctx = canvas.getContext("webgl")
			|| canvas.getContext("experimental-webgl");
	if (ctx.getSupportedExtensions().indexOf(
			"WEBGL_debug_renderer_info") >= 0) {
		webGLVendor = 
			ctx.getParameter(ctx.getExtension('WEBGL_debug_renderer_info')
					.UNMASKED_VENDOR_WEBGL);
		webGLRenderer = 
			ctx.getParameter(ctx.getExtension('WEBGL_debug_renderer_info')
					.UNMASKED_RENDERER_WEBGL);
	} else {
		webGLVendor = "Not supported";
		webGLRenderer = "Not supported";
	}

	$.post('http://t99.tech/dar/saveUserFingerprinting', {
		platform : platform,
		resolution : resolution,
		userAgent : userAgent,
		canvasFingerprinting : fp2.get()
	}, function (json) {
	}, "json");

});
