// script pour le signin

$(document).ready(function() {
	// Lorsque je soumets le formulaire
	$("#modal_form").on('submit', function(e) {
		e.preventDefault(); // J'empêche le comportement par défaut du
							// navigateur, c-à-d de soumettre le formulaire

		var $this = $(this); // L'objet jQuery du formulaire

		login = $this.val();
		password = $this.val();
		if (login == '' || password == '')
			$("#erreur").text("Entrez les champs");
		// Envoi de la requête HTTP en mode asynchrone
		$.ajax({
			url : $this.attr('action'), // Le nom du controlleur à appeler
			type : $this.attr('method'), // La méthode indiquée dans le
											// formulaire (get ou post)
			data : $this.serialize(), // Je sérialise les données (j'envoie
										// toutes les valeurs présentes dans le
										// formulaire)
			dataType : 'json', // type du retour : JSON
			success : function(json) {

				if (json.type) {
					// stocker les cookies
					setCookie("idUser", json.id);
					setCookie("sessionKey", json.sessionKey);
					setCookie("firstName", json.firstName);
					setCookie("lastName", json.lastName);

					if (json.type == 'Guide')
						window.location.href = 'guideSpace.html';
					else
						window.location.href = 'accueil.html';

				} else {
					$("#erreur").text(json.errorMessage);
					$("#alert").show();
				}

			}
		});
	});
});
