$("#guides").click(function(event){
 
  //disable the default form submission
  event.preventDefault();
  chargerGuides();
  
});
function chargerGuides(){

	$.getJSON( "chargerLesGuides", function( data ) {
		  var items = [];
		  $.each( data, function( key, val ) {
			items.push( "<tr>" );
		    items.push( "<td id='" + key.id + "'>" + val.id + "</td>" );
		    items.push( "<td id='" + key.nomUser + "'>" + val.nomUser + "</td>" );
		    items.push( "<td id='" + key.prenomUser + "'>" + val.prenomUser + "</td>" );
		    items.push( "<td id='" + key.age + "'>" + val.age + "</td>" );
		    items.push( "<td id='" + key.login + "'>" + val.login + "</td>" );
		    items.push( "<td id='" + key.password + "'>" + val.password + "</td>" );
		    items.push( "<td id='" + key.isGuide + "'>" + val.isGuide + "</td>" );
		    items.push( "<td><a href=\"approuverGuide?idGuide="+val.id+"\">Approuver</a></td>" );
		    items.push( "</tr>" );
		  });
		 
		  $("#listeDesVisites").hide();
		  $("#listeDesGuides").show();
		  $("#tableGuide").append(items);
		  //items = [];
		});
}