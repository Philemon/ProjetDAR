$(document).ready( function() {
 
   $('#location').autocomplete({
    source : function(request, response){ // les deux arguments représentent les données nécessaires au plugin
	$.ajax({
		    type : 'GET', // envoi des données en GET ou POST
		    url : 'findVisitsByLocation' , // url du controller de traitement
            data : 'location='+$("#location").val() , // données à envoyer en  GET ou POST            
            dataType: 'json', // JSON
            success : function(json){
                response($.map(json.visit, function(objet){
                	
                	if (objet.id) {
                	  var items = [];
            		  $.each( json.visit, function(key, val ) {
            			  
            			  
            			  if (val.typeTemp) {
              		    	
              		    	switch (val.typeTemp) {
      						case 'Cloudy':
      							 items.push( "<p><img  alt=\"\"  src=\"resources/images/rain.png\"\></p>" );
      							break;
      							
      						case 'Showers':
      							items.push( "<p><img  alt=\"\"  src=\"resources/images/rain.png\"\></p>" );
      							break;
      						
      						case 'Scattered' :
      							items.push( "<p><img  alt=\"\"  src=\"resources/images/rain.png\"\></p>" );
      							break;
      							
      						case 'Mostly' :
      							items.push( "<p><img  alt=\"\"  src=\"resources/images/rain.png\"\></p>" );
      							break;
      							

      						default:
      							break;
      						}
            			  }
            			  
            			  
            			  
            			items.push( "<p>"+val.name+"</p>"+"<p>"+val.description+"</p>"+"<p>"+val.locName+"</p>"+
                    			"<img with=\"100\" height=\"100\" alt=\"\" src=\"getPicture?visiteId="+val.id+"\">&nbsp;<a href=\"#\"  onclick=\"participateFunction("+val.id+");return true;\">rejoignez-nous<a/><br>" );
            			// positionner la ville sur le map
                    	init_map(val.lat,val.lng,val.locName,val.name);   
                    	
            		  });
                	
                	
            		  $("#visit_content").html(items);
                	
                	
                	
                	return objet.locName; // on retourne cette forme de suggestion
                	}
                	else{
                		 $("#visit_content").html("Nous n'avons pas de visite pour ce lieu");
                	}
                }));
            }
        });
    }
});
} );