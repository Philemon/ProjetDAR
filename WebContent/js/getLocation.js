$(document).ready( function() {
$('#location').autocomplete({
    source : function(request, response){ // les deux arguments représentent les données nécessaires au plugin
	$.ajax({
		    type : 'GET', // envoi des données en GET ou POST
		    url : 'getLocation' , // url du controller de traitement
            data : 'location='+$("#location").val() , // données à envoyer en  GET ou POST            
            dataType: 'json', // JSON
            success : function(json){
                response($.map(json.results, function(objet){
                    
                	$("#lng").val(objet.geometry.location.lng);
                	$("#lat").val(objet.geometry.location.lat);
                	return objet.formatted_address; // on retourne cette forme de suggestion
                }));
            }
        });
    }
});
} );